// sorted_array.h

#ifndef _SORTED_ARRAY_H_
#define _SORTED_ARRAY_H_

#include <vector>

class SortedArray {
 public:
  SortedArray();
  ~SortedArray();

  void AddNumber(int num);

  std::vector<int> GetSortedAscending() const;
  std::vector<int> GetSortedDescending() const;
  int GetMax() const;
  int GetMin() const;

 private:
  std::vector<int> numbers_;
};

#endif  // _SORTED_ARRAY_H_
