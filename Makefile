# Makefile

all: simple_int_set message_book sorted_array draw_shape

simple_int_set: simple_int_set.h simple_int_set.cc simple_int_set_main.cc
	g++ -o simple_int_set simple_int_set.cc simple_int_set_main.cc

message_book: message_book.h message_book.cc message_book_main.cc
	g++ -o message_book message_book.cc message_book_main.cc

sorted_array: sorted_array.h sorted_array.cc sorted_array_main.cc
	g++ -o sorted_array sorted_array.cc sorted_array_main.cc

draw_shape: draw_shape.h draw_shape.cc draw_shape_main.cc
	g++ -o draw_shape draw_shape.cc draw_shape_main.cc

