// simple_int_set.h

#ifndef _SIMPLE_INT_SET_H_
#define _SIMPLE_INT_SET_H_

#include <iostream>
#include <set>

std::set<int> SetIntersection(const std::set<int>& set0,
                              const std::set<int>& set1);
std::set<int> SetUnion(const std::set<int>& set0,
                       const std::set<int>& set1);
std::set<int> SetDifference(const std::set<int>& set0,
                            const std::set<int>& set1);

bool InputSet(std::istream& in, std::set<int>* s);
void OutputSet(std::ostream& out, const std::set<int>& s);

#endif  // _SIMPLE_INT_SET_H_
